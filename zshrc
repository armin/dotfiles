# enable colors
autoload -U colors && colors

# set PS1
PS1="%{$fg[blue]%}%n%{$reset_color%}%{$fg[cyan]%}@%{$fg[blue]%}%m %{$fg[yellow]%}%~ %{$reset_color%}%(?.%F{green}.%F{red})=%{$reset_color%} "

# env / penv
. ~/.env
. ~/.penv

# force $TERM on rxvt
if [[ "$COLORTERM" == "rxvt-xpm" ]]; then
  export TERM="rxvt-unicode-256color"
fi

# force $TERM on xfce4-terminal
if [[ "$COLORTERM" == "xfce4-terminal" ]]; then
  export TERM="xterm-256color"
fi

# set $PATH
export PATH="$PATH:/usr/local/bin:/usr/local/sbin:/usr/sbin:/sbin"

# set standard editor via $EDITOR
if hash vim >/dev/null 2>&1; then
  export EDITOR='vim'
else
	export EDITOR='vi'
fi

# fix for ssh host completion from ~/.ssh/config (yes, this is ugly, sorry for this)
[ -f ~/.ssh/config ] && : ${(A)ssh_config_hosts:=${${${${(@M)${(f)"$(<~/.ssh/config)"}:#Host *}#Host }:#*\**}:#*\?*}}

# needed to keep backgrounded jobs running
setopt NO_HUP

# ls / grep / less / diff
case "$(uname)" in
  FreeBSD|OpenBSD|NetBSD|Darwin)
    ls_command="ls"
    if type gls >/dev/null 2>&1; then
      ls_command="gls"
    else
      echo "ERROR: gls override requested detected, but no \"coreutils\" package installed."
    fi
  ;;
esac
alias ls="$ls_command --color=auto -F --group-directories-first"
dircolors_command="dircolors"

if [[ "$TERM" != "dumb" ]]; then
  if [[ "$TERM" != "linux" ]]; then

    export LESS_TERMCAP_mb=$'\e[01;31m'
    export LESS_TERMCAP_md=$'\e[01;38;5;74m'
    export LESS_TERMCAP_me=$'\e[0m'
    export LESS_TERMCAP_se=$'\e[0m'
    export LESS_TERMCAP_so=$'\e[38;5;246m'
    export LESS_TERMCAP_ue=$'\e[0m'
    export LESS_TERMCAP_us=$'\e[04;34;5;146m'

    # NOTE: this sets $LS_COLORS as well:
    if hash "$dircolors_command" >/dev/null 2>&1; then
      if [[ -f $HOME/.local/nord.dircolors ]]; then
        eval "$("$dircolors_command" $HOME/.local/nord.dircolors)"
      else
        echo "no dircolors file found, downloading one (nord) ..."
        wget -O $HOME/.local/nord.dircolors https://raw.githubusercontent.com/arcticicestudio/nord-dircolors/develop/src/dir_colors
        echo "activating ..."
        eval "$("$dircolors_command" $HOME/.local/nord.dircolors)"
      fi
    fi
    export LS_COLORS
    # Use ggrep on OpenBSD if installed
    if [[ "$(uname)" == OpenBSD ]]; then
      if type ggrep >/dev/null 2>&1; then
        alias grep="ggrep --color=auto"
      fi
    else
      alias grep="grep --color='auto'"
    fi

    alias less='less -R'
    alias diff='colordiff'
  fi
fi

# disable auto correction (sudo)
alias sudo='nocorrect sudo'

# disable auto correction (global)
unsetopt correct{,all} 

# don't select first tab menu entry
unsetopt menu_complete

# disable flowcontrol
unsetopt flowcontrol

# enable tab completion menu
setopt auto_menu

# enable in-word completion
setopt complete_in_word
setopt always_to_end

# word characters
WORDCHARS='*?_-.[]~=&;!#$%^(){}<>'

# load complist mod
zmodload -i zsh/complist

# completion list color definitions
zstyle ':completion:*' list-colors ''

# enable in-menu keybindings
bindkey -M menuselect '^o' accept-and-infer-next-history
zstyle ':completion:*:*:*:*:*' menu select
zstyle ':completion:*:*:*:*:*' list-colors '=(#b) #([0-9]#) ([0-9a-z-]#)*=00;33=0=01'
zstyle ':completion:*:*:kill:*:processes' list-colors '=(#b) #([0-9]#) ([0-9a-z-]#)*=01;34=0=01'
zstyle ':completion:*:*:*:*:processes' command "ps -u `whoami` -o pid,user,comm -w -w"

# disable named-directories autocompletion
zstyle ':completion:*:cd:*' tag-order local-directories directory-stack path-directories
cdpath=(.)

# make completion case-insensitive
zstyle ':completion:*' matcher-list '' 'm:{a-zA-Z}={A-Za-z}'

# allow extended globbing
setopt extendedglob

# don't complete first match (wildcard match)
zstyle '*' single-ignored show

# enable completion system (-i: disable check for insecure
# files/directories)
autoload -U compinit && compinit -i

# use expand-or-complete-with-dots
zle -N expand-or-complete-with-dots
expand-or-complete-with-dots() {
    echo -n "\e[36m⌛\e[0m"
    zle expand-or-complete
    zle redisplay
}

# completion thing
bindkey "^I" expand-or-complete-with-dots

# load "select-word-style"
autoload -Uz select-word-style

# it's magic!
select-word-style bash

# enable backward-kill-word-match
zle -N backward-kill-word-match

# history options 
HISTSIZE=2000
HISTFILE="$HOME/.history"
SAVEHIST=$HISTSIZE
setopt hist_ignore_all_dups

# automatically cd to dir without "cd" needed
setopt autocd

# this let's us select keymaps (command prompt input mode)
zle -N zle-keymap-select

# use emacs line editing (command prompt input mode)
bindkey -e

# fix home/end keys
bindkey '\e[1~' beginning-of-line
bindkey '\e[4~' end-of-line
bindkey "^[[H" beginning-of-line
bindkey "^[[F" end-of-line

# fix delete key
bindkey "^[[3~" delete-char

# share history among sessions?
#setopt share_history

# force en_US locale
export LC_ALL=en_US.UTF-8
export LANG=en_US.UTF-8

# fix $SHELL
if [[ ! "$SHELL" =~ .*zsh ]]; then
  export SHELL="$(which $0)"
fi

# make shift-tab work in completion menus
bindkey -M menuselect '^[[Z' reverse-menu-complete

# fix ssh host completion
zstyle ':completion:*:ssh:*' hosts off

# syntax highlighting
# OpenBSD
#source /usr/local/share/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
# others
source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh

autoload -Uz vcs_info
zstyle ':vcs_info:*' enable git svn
precmd() {
    vcs_info
}

setopt prompt_subst
##### PS1="%F{4}%n%F{111}@%F{4}%m %F{215}%~ %(?.%F{green}.%F{red})$%{$reset_color%} "
RPROMPT='%F{243}$(date)${vcs_info_msg_0_} '

zstyle ':vcs_info:git*' formats "%{$fg[grey]%}%s %{$reset_color%}%r/%S%{$fg[grey]%} %{$fg[blue]%}%b%{$reset_color%}%m%u%c%{$reset_color%} "

# command not found (pacman)
if [[ -e /usr/bin/pacman ]]; then
function command_not_found_handler {
    local purple='\e[1;35m' bright='\e[0;1m' green='\e[1;32m' reset='\e[0m'
    printf "zsh: ${bright}command not found${reset}: ${bright}${green}%s${reset} - searching package database..." "$1"
    local entries=(
        ${(f)"$(/usr/bin/pacman -F --machinereadable -- "/usr/bin/$1")"}
    )
    if (( ${#entries[@]} ))
    then
        printf "\n${bright}$1${reset} may be found in the following packages:\n"
        local pkg
        for entry in "${entries[@]}"
        do
            # (repo package version file)
            local fields=(
                ${(0)entry}
            )
            if [[ "$pkg" != "${fields[2]}" ]]
            then
                printf "${purple}%s/${bright}%s ${green}%s${reset}\n" "${fields[1]}" "${fields[2]}" "${fields[3]}"
            fi
            printf '    /%s\n' "${fields[4]}"
            pkg="${fields[2]}"
        done
    else
      printf "\n"
    fi
    return 127
}
fi

# window title stuffz
'builtin' 'typeset' -g __zwt_dir && \
	__zwt_dir=${0:A:h}
'builtin' 'typeset' -g +r ZWT_VERSION >/dev/null && \
	ZWT_VERSION=1.0.2 && \
	'builtin' 'typeset' -gr ZWT_VERSION
'builtin' 'typeset' -gi +r __zsh_window_title_debug_default >/dev/null && \
	__zsh_window_title_debug_default=0 && \
	'builtin' 'typeset' -gir __zsh_window_title_debug_default
'builtin' 'typeset' -gi +r __zsh_window_title_directory_depth_default >/dev/null && \
	__zsh_window_title_directory_depth_default=2 && \
	'builtin' 'typeset' -gir __zsh_window_title_directory_depth_default
'builtin' 'typeset' -gi +r __zwt_debug_default >/dev/null && \
	__zwt_debug_default=0 && \
	'builtin' 'typeset' -gir __zwt_debug_default
__zsh-window-title:add-hooks() {
	'builtin' 'emulate' -LR zsh
	add-zsh-hook precmd __zsh-window-title:precmd
	add-zsh-hook preexec __zsh-window-title:preexec
}
__zsh-window-title:init() {
	'builtin' 'emulate' -LR zsh
	'builtin' 'typeset' -gi ZWT_DEBUG=${ZWT_DEBUG:-$__zwt_debug_default}
	'builtin' 'typeset' -gi ZSH_WINDOW_TITLE_DEBUG=${ZSH_WINDOW_TITLE_DEBUG:-$__zsh_window_title_debug_default}
	'builtin' 'typeset' -gi ZSH_WINDOW_TITLE_DIRECTORY_DEPTH=${ZSH_WINDOW_TITLE_DIRECTORY_DEPTH:-$__zsh_window_title_directory_depth_default}
	__zsh-window-title:precmd
	'builtin' 'autoload' -U add-zsh-hook
	__zsh-window-title:add-hooks
}
__zsh-window-title:precmd() {
	'builtin' 'emulate' -LR zsh
	local title=$(print -P "%$ZSH_WINDOW_TITLE_DIRECTORY_DEPTH~")
	'builtin' 'echo' -ne "\033]0;[zsh] ${USER}@${HOST}:$title\007"
}
__zsh-window-title:preexec() {
	'builtin' 'emulate' -LR zsh
	local title=$(print -P "%$ZSH_WINDOW_TITLE_DIRECTORY_DEPTH~ - ${1[(w)1]}")
	'builtin' 'echo' -ne "\033]0;$title\007"
}
__zsh-window-title:init

# enable command line editing with ^E^X (like in BASH)
autoload -z edit-command-line
zle -N edit-command-line
bindkey "^X^E" edit-command-line



