[Appearance]
ColorScheme=Breeze
Font=Hack,13,-1,7,50,0,0,0,0,0

[Cursor Options]
CustomCursorColor=194,85,77
UseCustomCursorColor=true

[General]
Command=zsh
LocalTabTitleFormat=%w [d: %d] : [n: %n]
Name=Armin
Parent=FALLBACK/
RemoteTabTitleFormat=%w [u: %u] [H: %H]
ShowTerminalSizeHint=false
StartInCurrentSessionDir=false
TerminalColumns=112
TerminalMargin=12
TerminalRows=32

[Interaction Options]
ColorFilterEnabled=false

[Scrolling]
HighlightScrolledLines=false
HistorySize=10000
ReflowLines=false

[Terminal Features]
BellMode=3
