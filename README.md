dotfiles - My digital junk drawer, now in Git form
==================================================

<img src="junk.jpg" alt="Junk Drawer" width="300" height="300">

This is a personal junk dump repository and might perfectly well contain something that needs further tweaking or small changes in order to run on some new setup. Configuration files for various UNIX/Linux utilities I use every day. I will try to keep this repository as clean and minimal as anyhow possible, but we all know how this goes sometimes. In the hope that this will be useful for someone else, this is the public online copy mirror of what I consider "life-saver" configs (or whatever you choose to call it...).

