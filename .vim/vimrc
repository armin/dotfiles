set nocompatible

set mouse=
syn on
set bg=dark
set et
set bs=2
set tabstop=2
set softtabstop=2
set shiftwidth=2
au BufReadPost * if line("'\"") > 0|if line("'\"") <= line("$")|exe("norm '\"")|else|exe "norm $"|endif|endif
set hlsearch
if !isdirectory(expand('~/.vim')) | call mkdir(expand('~/.vim'), 'p') | endif
if !isdirectory(expand('~/.vim/swap')) | call mkdir(expand('~/.vim/swap'), 'p') | endif
se nobackup writebackup
set directory=~/.vim/swap
set backupdir=~/.vim/swap
set shortmess=I
hi StatusLine ctermbg=252 ctermfg=black
set laststatus=2
set number
" colorscheme nord
" colorscheme Benokai
colorscheme bvemu

set statusline=%f                          " File name
set statusline+=%y                         " File type
set statusline+=%m                         " Modified flag [+] if modified
set statusline+=%r                         " Read-only flag [RO] if read-only
set statusline+=%=                         " Right-align the next section
set statusline+=%l/%L                      " Current line / Total lines
set statusline+=\ [%p%%]                   " Percentage through the file
set statusline+=%c                         " Column number
set laststatus=2     

set wildmenu
set wildoptions=pum
set wildmode=full

set showmatch
set matchtime=2

filetype plugin on

" --- statusline stuff ---
set statusline=
" filename, relative to cwd
set statusline+=%f
" separator
set statusline+=\ 
" modified flag
set statusline+=%#wildmenu#
set statusline+=%m
set statusline+=%*
"Display a warning if file encoding isnt utf-8
set statusline+=%#question#
set statusline+=%{(&fenc!='utf-8'&&&fenc!='')?'['.&fenc.']':''}
set statusline+=%*
"display a warning if fileformat isnt unix
set statusline+=%#directory#
set statusline+=%{&ff!='unix'?'['.&ff.']':''}
set statusline+=%*
"display a warning if files contains tab chars
set statusline+=%#warningmsg#
set statusline+=%{StatuslineTabWarning()}
set statusline+=%*
" read-only
set statusline+=%r
set statusline+=%*
" right-align
set statusline+=%=
" filetype
set statusline+=[%{strlen(&ft)?&ft:'none'}]
" separator
set statusline+=\ 
" current char
set statusline+=[%b],[0x%02B]
" separator
set statusline+=\ 
" column,
set statusline+=(C:%2c),
" current line / lines in file
set statusline+=[%l/%L]
" always show status line
set laststatus=2
" return '[tabs]' if tab chars in file, or empty string
function! StatuslineTabWarning()
    if !exists("b:statusline_tab_warning")
        let tabs = search('^\t', 'nw') != 0

        if tabs
            let b:statusline_tab_warning = '[tabs]'
        else
            let b:statusline_tab_warning = ''
        endif
    endif
    return b:statusline_tab_warning
endfunction
"recalculate the tab warning flag when idle and after writing
autocmd cursorhold,bufwritepost * unlet! b:statusline_tab_warning

